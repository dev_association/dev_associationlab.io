---
layout: post
title:  "std::array template argument deduction"
date:   2017-01-13
author: Mateusz Dudek
categories: c++
---
C++'s template deduction is a powerful tool that allows compiler to deduce template arguments from
function arguments.
This mechanism is often used to simplify code, or to leverage techniques that are normally
not possible, for example &mdash; template operators like `std::basic_ostream::operator<<`, since
C++ doesn't supply syntax for such calls without rewriting it as function call (which is yucky
`std::operator<< <std::char_traits<char>> (std::cout, "Yuck.");` ). We can also use it with 
standard containers to allow for more flexible construction.

Let's consider a dummy problem of storing pointers to member functions without worrying about their
signature. It's useful if we want to perform a series of validations on an object.
These validations are of fixed size, known at compile time, so run-time overhead is undesirable.
Validations will be iterated over, and called as a function pointer. While this is
by far not the best design it serves the purpose of presenting possible use case for `std::array`
template deduction. This is the kind of behavior we want to achieve:

{% highlight c++ %}
struct widget { };

struct validator
{
  bool first_validation(const widget& w) const;
  bool second_validation(const widget& w) const;
  bool third_validation(const widget& w) const;
};

int main()
{
  validator validator;
  widget widget;
  bool status = false;

  // what does validations() return?
  for (const auto fptr : validator.validations())
  {
    status = status && (validator.*fptr)(widget);
  }
};
{% endhighlight %}

We could consider returning validators as `std::vector` which solves the problem but has
several design issues. `std::vector` yields heap allocation which is unnecessary as in this case
validations won't change at run-time. So naturally we would like to be able to use `std::array`
which in fact is pretty straightforward:

{% highlight c++ %}
struct validator
{
  bool first_validation(const widget& w) const;
  bool second_validation(const widget& w) const;
  bool third_validation(const widget& w) const;

  auto validations() const
  {
    return std::array<bool(validator::*)(const widget& w) const, 3>
      {
        &validator::first_validation,
        &validator::second_validation,
        &validator::third_validation
      };
  }
};
{% endhighlight %}

There are several problems with this approach though. First of all the size of the container
must be specified explicitly by the programmer. Besides &mdash; having to worry
about validations signature is frustrating and error prone. What we would
like to do is leave array size and `value_type` deduction to the compiler, and write something
like this:

{% highlight c++ %}
auto validations() const
{
  return make_array(
    &validator::first_validation,
    &validator::second_validation,
    &validator::third_validation
  );
}
{% endhighlight %}

Thankfully we can solve this using [parameter pack](http://en.cppreference.com/w/cpp/language/parameter_pack)
and operator [`sizeof...`](http://en.cppreference.com/w/cpp/language/sizeof...) Parameter pack  is a 
template parameter that accepts zero or more function arguments.
`sizeof...` operator is used specifically with parameter pack to get number of elements in a pack.
That allows us to pass any number of arguments to `make_array` function.
What's left is querying the common type of all types `T...` &mdash; 
[`std::common_type`](http://en.cppreference.com/w/cpp/types/common_type) comes in handy.

{% highlight c++ %}
template <typename... Ts>
auto make_array(Ts&&... ts)
  -> std::array<std::common_type_t<Ts...>, sizeof...(ts)>
{
  // implementation
}
{% endhighlight %}

We now have `make_array` function which underlying type is deduced by the compiler, so that
we don't have to worry about it explicitly. Let's forward parameters to `std::array`
aggregate constructor (because `T&&` is a [universal reference](https://isocpp.org/blog/2012/11/universal-references-in-c11-scott-meyers)) 
and we have complete `make_array` definition.

{% highlight c++ %}
template <typename... Ts>
auto make_array(Ts&&... ts)
  -> std::array<std::common_type_t<Ts...>, sizeof...(ts)>
{
  return {std::forward<Ts>(ts)...};
}
{% endhighlight %}

This implementation is a primitive one, and in library fundamentals TS v2 extension
comes much more [robust implementation](http://en.cppreference.com/w/cpp/experimental/make_array), 
but for now, this is one way of creating automatically deduced `std::array`.

---------------------------------------

<br>
In **c++17** however we have an additional way of initialization without having to specify template
arguments explicitly. [Template argument deduction for class templates](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2016/p0091r3.html)
proposal allows us to initialize `std::array` directly

{% highlight c++ %}
#include <array>
#include <type_traits>

// deduction guide
namespace std
{
template <typename... Ts> array(Ts... ts)
  -> array<std::common_type_t<Ts...>, sizeof...(ts)>;
}

class widget;
struct validator
{
  bool first_validation(const widget& w) const;
  bool second_validation(const widget& w) const;
  bool third_validation(const widget& w) const;

  auto validations() const
  {
    return std::array{
      &validator::first_validation,
      &validator::second_validation,
      &validator::third_validation};
  }
};
{% endhighlight %}

The syntax `explicit(optional) template-name ( parameter-declaration-clause ) -> simple-template-id ;`
declares a user-defined deduction guide, which participates in deduction and overload resolution.
Deduction-guides are also created implicitly, but for `std::array` no overloaded constructor can be
deduced for single template pack `Ts...`, as `std::array` declaration takes type `T` and value `N`
of type `std::size_t`, whereas our template pack is expanded as multiple types derived from passed
arguments, so without our user-defined guide the program is ill-formed.

----------------------------------------

<small>
Sources:  
\[1\] [en.cppreference.com](http://en.cppreference.com)  
\[2\] [Template argument deduction for class templates (Rev. 6)](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2016/p0091r3.html)
</small>
