---
layout: page
title: About
permalink: /about/
---

**\>dev** is a students' association located in Cracow University of Economics. This blog serves as
a notepad for whatever we might possibly think of that's programing related. You can check our
[website](https://dev.uek.krakow.pl) (if it's not currently down), [gitlab](https://gitlab.com/dev_association) 
and [github](https://github.com/KN-DEV) pages.
